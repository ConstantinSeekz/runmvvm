package com.example.runmvvm.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.runmvvm.data.repositories.UserRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock

@RunWith(JUnit4::class)
class AuthViewModelTest {

    private val login = "e@e.com"
    private val pass = "1111"
    private var userRepository : UserRepository? = null

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    private var repository = mock(UserRepository::class.java)
    private lateinit var viewmodel: AuthViewModel

    @Before
    fun init(){
        viewmodel = AuthViewModel(repository)
//        userRepository = UserRepository(HttpManagerService.getInstance())
    }

    @Test
    fun loginSuccess(){

        Assert.assertNotNull(viewmodel)
        repository.userLogin("test","test")


    }

//    @Test
//    fun loginError(){
//
//    }

}