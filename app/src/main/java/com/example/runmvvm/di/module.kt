package com.example.runmvvm.di

import com.example.runmvvm.data.networking.HttpManagerService
import com.example.runmvvm.data.repositories.UserRepository
import com.example.runmvvm.ui.viewmodel.AuthViewModel
import org.koin.dsl.module

val authModule = module {

    single { HttpManagerService.getInstance() }
    factory { UserRepository( get() ) }
    single { AuthViewModel( get() )  }
}