package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class JoinEventResponse {

    @SerializedName("data")
    @Expose
//    val data: JoinEventData? = null
    val data: List<Objects>? = null
    @SerializedName("status")
    @Expose
    val status: Status? = null
}