package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RunEventDetailData {

    @SerializedName("id")
    @Expose
    val id: Int? = null
    @SerializedName("event_code")
    @Expose
    val eventCode: String? = null
    @SerializedName("event_name")
    @Expose
    val eventName: String? = null
    @SerializedName("event_organized_by")
    @Expose
    val eventOrganizedBy: String? = null
    @SerializedName("event_detail")
    @Expose
    val eventDetail: String? = null
    @SerializedName("event_entry_type")
    @Expose
    val eventEntryType: String? = null
    @SerializedName("event_image")
    @Expose
    val eventImage: String? = null
    @SerializedName("event_date_left")
    @Expose
    val eventDateLeft: Int? = null
    @SerializedName("event_other_info")
    @Expose
    val eventOtherInfo: List<Any>? = null
    @SerializedName("event_challenge")
    @Expose
    val eventChallenge: List<EventChallengeData>? = null
}