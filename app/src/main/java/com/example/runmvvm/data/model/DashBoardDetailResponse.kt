package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DashBoardDetailResponse {

    @SerializedName("data")
    @Expose
    val data: DashBoardData? = null
    @SerializedName("status")
    @Expose
    val status: Status? = null
}