package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class EventChallengeData {

    @SerializedName("challenge_id")
    @Expose
    val challengeId: Int? = null
    @SerializedName("rule_group_id")
    @Expose
    val ruleGroupId: Int? = null
    @SerializedName("rule_id")
    @Expose
    val ruleId: Int? = null
    @SerializedName("challenge_name")
    @Expose
    val challengeName: String? = null
    @SerializedName("entry_fee")
    @Expose
    val entryFee: Int? = null
    @SerializedName("entry_fee_unit")
    @Expose
    val entryFeeUnit: String? = null
    @SerializedName("progression_type")
    @Expose
    val progressionType: String? = null
    @SerializedName("rule_type")
    @Expose
    val ruleType: String? = null
    @SerializedName("distance")
    @Expose
    val distance: Int? = null
    @SerializedName("distance_unit")
    @Expose
    val distanceUnit: String? = null

}