package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class Status {

    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("message")
    @Expose
    val message: String? = null
}