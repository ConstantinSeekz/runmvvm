package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterData {

    @SerializedName("id")
    @Expose
    val id: Int? = null
    @SerializedName("runner_id")
    @Expose
    val runnerId: String? = null
    @SerializedName("email")
    @Expose
    val email: String? = null
    @SerializedName("user_token")
    @Expose
    val userToken: String? = null
    @SerializedName("first_name")
    @Expose
    val firstName: String? = null
    @SerializedName("last_name")
    @Expose
    val lastName: String? = null
    @SerializedName("display_name")
    @Expose
    val displayName: String? = null
    @SerializedName("telephone_number")
    @Expose
    val telephoneNumber: String? = null
    @SerializedName("image")
    @Expose
    val image: String? = null
    @SerializedName("address")
    @Expose
    val address: String? = null
    @SerializedName("date_of_birth")
    @Expose
    val dateOfBirth: String? = null
    @SerializedName("gender")
    @Expose
    val gender: String? = null
    @SerializedName("height")
    @Expose
    val height: Int? = null
    @SerializedName("weight")
    @Expose
    val weight: String? = null
    @SerializedName("created_at")
    @Expose
    val createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    val updatedAt: String? = null
//    @SerializedName("id")
//    @Expose
//    val id: Int? = null
//    @SerializedName("runner_id")
//    @Expose
//    val runnerId: String? = null
//    @SerializedName("email")
//    @Expose
//    val email: String? = null
//    @SerializedName("user_token")
//    @Expose
//    val userToken: String? = null
//    @SerializedName("first_name")
//    @Expose
//    val firstName: String? = null
//    @SerializedName("last_name")
//    @Expose
//    val lastName: String? = null
//    @SerializedName("display_name")
//    @Expose
//    val displayName: String? = null
//    @SerializedName("telephone_number")
//    @Expose
//    val telephoneNumber: String? = null
//    @SerializedName("image")
//    @Expose
//    val image: String? = null
//    @SerializedName("address")
//    @Expose
//    val address: String? = null
//    @SerializedName("date_of_birth")
//    @Expose
//    val dateOfBirth: String? = null
//    @SerializedName("gender")
//    @Expose
//    val gender: String? = null
//    @SerializedName("height")
//    @Expose
//    val height: String? = null
//    @SerializedName("weight")
//    @Expose
//    val weight: String? = null
//    @SerializedName("created_at")
//    @Expose
//    val createdAt: String? = null
//    @SerializedName("updated_at")
//    @Expose
//    val updatedAt: String? = null
//    @SerializedName("other_info")
//    @Expose
//    val otherInfo: String? = null


}