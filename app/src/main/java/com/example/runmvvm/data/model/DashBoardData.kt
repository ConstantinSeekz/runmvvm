package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class DashBoardData {

    @SerializedName("event_id")
    @Expose
     val eventId: String? = null
    @SerializedName("event_code")
    @Expose
     val eventCode: String? = null
    @SerializedName("event_name")
    @Expose
     val eventName: String? = null
    @SerializedName("image")
    @Expose
     val image: String? = null
    @SerializedName("register_code")
    @Expose
     val registerCode: String? = null
    @SerializedName("goal")
    @Expose
     val goal: Float? = null
    @SerializedName("total_distance")
    @Expose
     val totalDistance: Float? = null
    @SerializedName("unit")
    @Expose
     val unit: String? = null
    @SerializedName("color1")
    @Expose
     val color1: String? = null
    @SerializedName("color2")
    @Expose
     val color2: String? = null
    @SerializedName("color3")
    @Expose
     val color3: String? = null
    @SerializedName("event_status")
    @Expose
     val eventStatus: String? = null
    @SerializedName("event_goal")
    @Expose
     val eventGoal: Float? = null
    @SerializedName("event_total_distance")
    @Expose
     val eventTotalDistance: Float? = null
    @SerializedName("date_left")
    @Expose
     val dateLeft: Int? = null
    @SerializedName("start_date")
    @Expose
     val startDate: String? = null
    @SerializedName("end_date")
    @Expose
    val endDate: String? = null

}