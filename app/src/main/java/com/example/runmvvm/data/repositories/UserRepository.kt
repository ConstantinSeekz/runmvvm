package com.example.runmvvm.data.repositories

import androidx.lifecycle.MutableLiveData
import com.example.runmvvm.data.model.RegisterResponse
import com.example.runmvvm.data.networking.HttpManagerService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.google.gson.Gson
import java.io.IOException

class UserRepository(private var httpService : HttpManagerService) {

    private val AUTH = "5PI0Os4v5pru1EstLcOS"
    fun userLogin(email: String, password: String) : MutableLiveData<RegisterResponse>{

        val loginResponse = MutableLiveData<RegisterResponse>()

       val call =  httpService.service.userLogin(AUTH, email, password)

        call.enqueue(object : Callback<RegisterResponse?> {

            override fun onResponse(call: Call<RegisterResponse?>, response: Response<RegisterResponse?>) {

                    val code = response.code()

                    if(code == 200) {
                        loginResponse.value = response.body()
                    }
                    else if (response.code() == 400) {
                        val gson = Gson()
                        val adapter = gson.getAdapter(RegisterResponse::class.java)
                        try {
                            if (response.errorBody() != null)
                                loginResponse.value = adapter.fromJson(
                                    response.errorBody()!!.string()
                                )
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                }

                override fun onFailure(call: Call<RegisterResponse?>, t: Throwable) {
                }


            })

        return loginResponse

    }
}