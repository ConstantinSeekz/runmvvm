package com.example.runmvvm.data.model

import com.example.earthdev.runmoreround.model.RegisterData
import com.example.earthdev.runmoreround.model.Status
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class RegisterResponse {

    @SerializedName("data")
    @Expose
    val data: RegisterData? = null
//    var user: Array<User>? = null
    @SerializedName("status")
    @Expose
    val status: Status? = null
}