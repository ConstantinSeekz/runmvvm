package com.example.runmvvm.data.networking;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HttpManagerService {

    private static HttpManagerService instance;

    public static HttpManagerService getInstance() {
        if (instance == null)
            instance = new HttpManagerService();
        return instance;
    }

    private ApiService service;

    private HttpManagerService() {

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.runmoreround.com/api/")
//                .baseUrl("http://203.150.90.1/runmoreround/")
                .client(clientBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

         service = retrofit.create(ApiService.class);
    }

    public ApiService getService(){
        return service;
    }

}
