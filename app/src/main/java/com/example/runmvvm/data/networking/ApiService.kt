package com.example.runmvvm.data.networking

import com.example.earthdev.runmoreround.model.*
import com.example.runmvvm.data.model.RegisterResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.POST


interface ApiService {

    @Multipart
    @POST("user/edit")
    abstract fun editProfile(
        @HeaderMap apiToken: Map<String, String>,
        @Part("user_id") userId: RequestBody,
        @Part("token") token: RequestBody,
        @Part("first_name") firstName: RequestBody,
        @Part("last_name") lastName: RequestBody,
        @Part("display_name") displayName: RequestBody,
        @Part("telephone_number") phoneNumber: RequestBody,
        @Part file: MultipartBody.Part,
        @Part("address") address: RequestBody,
        @Part("date_of_birth") dateBirthDay: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part("height") height: RequestBody,
        @Part("weight") weight: RequestBody
    ): Call<EditProfileResponse>

//
    @FormUrlEncoded
    @POST("user/register")
    abstract fun register(
        @Header("api-token") apiToken: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<RegisterResponse>


    @FormUrlEncoded
    @POST("user/changePassword")
    abstract fun changePassword(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String,
        @Field("password") currentPassword: String,
        @Field("new_password") newPassword: String
    ): Call<JoinEventResponse>

    @FormUrlEncoded
    @POST("user/forgetPassword")
    abstract fun forgetPassword(
        @Header("api-token") apiToken: String,
        @Field("email") email: String
    ): Call<JoinEventResponse>

//    @FormUrlEncoded
//    @POST("login")
//    abstract fun userLogin(
////        @Header("api-token") apiToken: String,
//        @Field("email") email: String,
//        @Field("password") password: String
//    ): Call<RegisterResponse>

    @FormUrlEncoded
    @POST("login")
    fun userLogin(
        @Header("api-token") apiToken: String,
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<RegisterResponse>

    @FormUrlEncoded
    @POST("user")
    abstract fun getUser(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String
    ): Call<EditProfileResponse>

    @FormUrlEncoded
    @POST("event/all")
    fun getRunEvent(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String
    ): Call<RunEventResponse>

    @FormUrlEncoded
    @POST("event")
    fun getRunEventDetail(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String,
        @Field("event_id") eventId: String
    ): Call<RunEventDetailResponse>

    @FormUrlEncoded
    @POST("event/join")
    abstract fun joinEvent(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String,
        @Field("event_id") eventId: String,
        @Field("challenge_id") challengeId: String,
        @Field("rule_group_id") rule_groupId: String,
        @Field("rule_id") ruleId: String,
        @Field("distance") distance: String
    ): Call<JoinEventResponse>

    @FormUrlEncoded
    @POST("event/dashboards")
    abstract fun getDashBoardData(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String
    ): Call<DashBoardResponse>

    @FormUrlEncoded
    @POST("event/dashboard")
    abstract fun getDashBoardDetail(
        @Header("api-token") apiToken: String,
        @Field("user_id") userId: String,
        @Field("token") token: String,
    @Field("register_code") registerCode: String
    ): Call<DashBoardDetailResponse>

    @Multipart
    @POST("event/tracking")
    abstract fun addDistance(
        @HeaderMap apiToken: Map<String, String>,
        @Part("user_id") userId: RequestBody,
        @Part("token") token: RequestBody,
        @Part file: MultipartBody.Part,
        @Part("distance") distance: RequestBody,
        @Part("register_code") registerCode: RequestBody
    ): Call<JoinEventResponse>
}