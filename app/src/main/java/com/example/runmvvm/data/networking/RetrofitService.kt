package com.example.runmvvm.data.networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService {

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://newsapi.org/v2/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun <S> cteateService(serviceClass: Class<S>): S {
        return retrofit.create(serviceClass)
    }
}

//class SearchSingleton {
//
//    //    private var searchObject: SearchObject? = null
//    private var searchObject =  SearchObject()
//
//    companion object{
//        var instance = SearchSingleton()
//    }
//
//    fun setDataObject(searchObject: SearchObject){
//        this.searchObject = searchObject
//    }
//
//    fun getDataObject():SearchObject{
//        return this.searchObject
//    }
//}