package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RunEventDetailResponse {

    @SerializedName("data")
    @Expose
    val data: RunEventDetailData? = null
    @SerializedName("status")
    @Expose
    val status: Status? = null

}