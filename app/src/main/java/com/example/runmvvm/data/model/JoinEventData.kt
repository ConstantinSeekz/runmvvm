package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class JoinEventData {

    @SerializedName("distance")
    @Expose
    val distance: List<String>? = null
}