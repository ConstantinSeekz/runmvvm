package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class EditProfileResponse {
    @SerializedName("data")
    @Expose
    val data: EditProfileData? = null
    @SerializedName("status")
    @Expose
    val status: Status? = null
}