package com.example.earthdev.runmoreround.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class RunEventResponse {

    @SerializedName("data")
    @Expose
    val data: List<RunEventData>? = null
    @SerializedName("status")
    @Expose
    val status: Status? = null

}