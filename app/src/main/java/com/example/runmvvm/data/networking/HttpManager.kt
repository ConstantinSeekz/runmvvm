package com.example.runmvvm.data.networking

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object HttpManager {

    private val AUTH = "5PI0Os4v5pru1EstLcOS"
//    private const val BASE_URL = "http://203.150.90.1/runmoreround/"
    private const val BASE_URL = "https://www.runmoreround.com/api/"




    private val okHttpClient=  OkHttpClient.Builder()
//        .addInterceptor(loggingInterceptor).build()
            .addInterceptor { chain ->

                val loggingInterceptor: HttpLoggingInterceptor? = HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)

                val origin = chain.request()
                val requestBuilder = origin.newBuilder()
                    .addHeader("api-token", AUTH)
                    .method(origin.method(), origin.body())

                val request = requestBuilder.build()
                chain.proceed(request)
            }.build()

//    val clientBuilder = OkHttpClient.Builder()
//    val loggingInterceptor = HttpLoggingInterceptor()
//    loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
//    clientBuilder.addInterceptor(loggingInterceptor)

    val instance : ApiService by lazy {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        retrofit.create(ApiService::class.java)
    }
}