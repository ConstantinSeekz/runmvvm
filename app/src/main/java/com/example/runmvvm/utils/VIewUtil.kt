package com.example.runmvvm.utils

import android.content.Context
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast


fun Context.toast(message: String){
   Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun ProgressBar.show(window: Window) {
    
      visibility = View.VISIBLE
    window.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    
}

fun ProgressBar.hide(window: Window){
      visibility = View.GONE

    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}
