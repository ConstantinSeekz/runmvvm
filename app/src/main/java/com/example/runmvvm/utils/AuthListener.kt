package com.example.runmvvm.utils

import androidx.annotation.NonNull
import androidx.lifecycle.MutableLiveData
import com.example.runmvvm.data.model.RegisterResponse

interface AuthListener {

    fun onStarted()

    fun onSuccess(@NonNull loginResponse: MutableLiveData<RegisterResponse>)

    fun onFailure(message: String)
}