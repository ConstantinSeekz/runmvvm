package com.example.runmvvm.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.example.runmvvm.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}
