package com.example.runmvvm.ui.viewmodel

import android.view.View
import android.widget.EditText
import androidx.lifecycle.ViewModel
import com.example.runmvvm.data.repositories.UserRepository
import com.example.runmvvm.utils.AuthListener
import androidx.lifecycle.MutableLiveData



class AuthViewModel(private val userRepository: UserRepository) : ViewModel() {

    var isLoading = MutableLiveData<Boolean>()

    var email: String? = null
    var password: String? = null
    var authListener: AuthListener? = null
    //
    fun onLoginButtonClick(view: View) {

        authListener?.onStarted()

        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            //
            authListener?.onFailure("Invalid email or password")
            return
        }

        val loginResponse = userRepository.userLogin(email!!, password!!)
        authListener?.onSuccess(loginResponse)

//        val loginResponse = UserRepository(HttpManagerService.getInstance()).userLogin(email!!, password!!)
//        authListener?.onSuccess(loginResponse)
    }


    fun clearEditText(editText: EditText) {
        editText.text.clear()
    }


}