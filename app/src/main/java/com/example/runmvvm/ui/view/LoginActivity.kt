package com.example.runmvvm.ui.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.*
import com.example.runmvvm.utils.AuthListener
import com.example.runmvvm.R
import com.example.runmvvm.databinding.ActivityLoginBinding
import com.example.runmvvm.di.authModule
import com.example.runmvvm.data.model.RegisterResponse
import com.example.runmvvm.utils.hide
import com.example.runmvvm.utils.show

import com.example.runmvvm.utils.toast
import com.example.runmvvm.ui.viewmodel.AuthViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.startKoin

class LoginActivity : AppCompatActivity(), AuthListener {

    var status = ""

    private val authViewModel : AuthViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_login)

        startKoin {
            androidLogger()
            androidContext(this@LoginActivity)
            modules(authModule)
        }

        bindingData()

    }

    private fun bindingData() {

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_login
        )
//        val viewModel = VieหwModelProviders.of(this).get(AuthViewModel::class.java)

        binding.viewModel = authViewModel

//        viewModel.authListener = this
        authViewModel.authListener = this
    }

    override fun onSuccess(loginResponse: MutableLiveData<RegisterResponse>) {

        loginResponse.observe(this, Observer { data ->
            //            toast(""+data.status!!.message!!)
            Log.d("checkResponse", "t =$data")


            status = data.status!!.status!!
            toast("" + status)
            if (status == resources.getString(R.string.status_success)) {

                Handler().postDelayed({
                    progressBar.hide(window)
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                }, 1000)

            } else if (status == resources.getString(R.string.status_fail)) {
                progressBar.hide(window)
                authViewModel.clearEditText(edtPassword)
            }

        })


    }

    override fun onStarted() {
        progressBar.show(window)
    }

    override fun onFailure(message: String) {
        toast(message)
        progressBar.hide(window)
    }
}
