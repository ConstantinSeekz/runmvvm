package com.example.runmvvm.ui.viewmodel

import com.example.runmvvm.data.model.User

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

class UserViewModel(private val user: User) : ViewModel(){
    var username: ObservableField<String>? = ObservableField()
    var password: ObservableField<String>? = ObservableField()
}

//class UserViewModel(private val user: User) : Observer, BaseObservable() {
//
//    /// Register itself as the observer of Model
//    init {
//        user.addObserver(this)
//    }
//
//    /// Notify the UI when change event emitting from Model is received.
//    override fun update(p0: Observable?, p1: Any?) {
//
//        if (p1 is String) {
//
////            when (p1) {
////                "age" -> {
//////                    notifyPropertyChanged(R.id.txt_age)
////                }
////                "fistName", "lastName" -> {
//////                    notifyPropertyChanged(com.example.runmvvm.BR.)
////                }
////                "imageUrl" -> {
////
////                }
////                "tagLine" -> {
////
////                }
////                "gender" -> {
////
////                }
////                else -> {
////
////                }
////            }
//            if (p1 is String) {
//                if (p1 == "age") {
//                    notifyPropertyChanged(BR.age)
//                } else if (p1 == "firstName" || p1 == "lastName") {
//                    notifyPropertyChanged(BR.name)
//                } else if (p1 == "imageUrl") {
//                    notifyPropertyChanged(BR.imageUrl)
//                } else if (p1 == "tagline") {
//                    notifyPropertyChanged(BR.tagline)
//                } else if (p1 == "female") {
//                    notifyPropertyChanged(BR.gender)
//                }
//            }
//
//
//        }
//
//    }
//
//    val name: String
//    @Bindable get() {
//        return user.firstName + " " +user.lastName
//    }
//
//    val age: String
//        @Bindable get() {
//            return if (user.age <= 0) return ""
//            else String.format(Locale.ENGLISH, "%d years old", user.age)
//        }
//    val gender: String
//        @Bindable get() {
////            return if (user.gender) return "Female" else "Male"
//            return user.gender
//        }
//
//    val imageUrl: String
//        @Bindable get() {
//            return user.imageUrl
//        }
//
//    val tagline: String
//        @Bindable get() {
//            return "Tagline: " + user.tagline
//        }
//
//
//}