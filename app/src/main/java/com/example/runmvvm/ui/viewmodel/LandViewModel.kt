package com.example.runmvvm.ui.viewmodel

import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModel
import com.example.runmvvm.ui.view.LoginActivity

class LandViewModel : ViewModel() {

    private var typeBundle: String = ""

    fun onBtnRegister(view: View) {

    }

    fun onBtnLogin(view: View) {
        Intent(view.context, LoginActivity::class.java).also {
            view.context.startActivity(it)
        }
    }
}