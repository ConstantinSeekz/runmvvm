package com.example.runmvvm.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.runmvvm.R
import com.example.runmvvm.databinding.ActivityLandBinding
import com.example.runmvvm.ui.viewmodel.LandViewModel

class LandActivity : AppCompatActivity(){

    private var viewModel: LandViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_land)

        val binding: ActivityLandBinding = DataBindingUtil.setContentView(this, R.layout.activity_land)
        viewModel = ViewModelProviders.of(this).get(LandViewModel::class.java)
        binding.viewModel = viewModel


    }

}
